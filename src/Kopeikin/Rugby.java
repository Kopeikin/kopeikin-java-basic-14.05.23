
package Kopeikin;

import java.util.Random;

public class Rugby {
    public static void main(String[] args) {
        Random random = new Random();

        int[] teamOne = new int[25];
        for (int i = 0; i < teamOne.length; i++) {
            teamOne[i] = random.nextInt(23) + 18;
        }
        int[] teamSecond = new int[25];
        for (int i = 0; i < teamSecond.length; i++) {
            teamSecond[i] = random.nextInt(23) + 18;
        }
        System.out.println("Вік гравців першої команди: ");
        for (int i = 0; i < teamOne.length; i++) {
            System.out.println("Гравець " + (i+1) + ": " + teamOne[i] + " років");
        }
        System.out.println();

        System.out.println("Вік гравців другої команди: ");
        for (int i = 0; i < teamSecond.length; i++) {
            System.out.println("Гравець " + (i+1) + ": " + teamSecond[i] + " років");
        }
        System.out.println();

        int sumAgeTeamOne = 0;
        int sumAgeTeamSecond = 0;

        for (int j : teamOne) {
            sumAgeTeamOne += j;
        }
        int averageTeamAgeOne = sumAgeTeamOne / teamOne.length;

        for (int j : teamSecond) {
            sumAgeTeamSecond += j;
        }
        int averageTeamAgeSecond = sumAgeTeamSecond / teamSecond.length;

        System.out.println("Середній вік гравців першої команди: " + averageTeamAgeOne + " років");
        System.out.println("Середній вік гравців другої команди: " + averageTeamAgeSecond + " років");


    }
}
